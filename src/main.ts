import { bootstrap } from 'angular';
import { app } from './app/app.module';

bootstrap(document.documentElement, [app.name], { strictDi: true });