import { TitleService } from './core/title.service';

export class AppComponent implements ng.IComponentOptions {
    public controller: any;
    public template: string;

    constructor() {
        this.controller = AppComponentController;
        this.template = 
            `<div class='conatiner'>
                {{$ctrl.title}}
            </div>`;
    }
}

class AppComponentController implements ng.IController {
    static $inject = ['TitleService'];

    title = 'Hello world!';

    constructor(private _titleService: TitleService) {  }

    $onInit() {
        this._titleService.title = 'Welcome!';
    }
}