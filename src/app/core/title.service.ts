export class TitleService {
    static $inject = ['$rootScope'];

    constructor(private _$rootScope: any) { 
        _$rootScope.rootVM = {
            pageTitle: ''
        }
    }

    get title(): string {
        return this._$rootScope.rootVM.pageTitle;
    }
    set title(value: string) {
        this._$rootScope.rootVM.pageTitle = value;
    }
}