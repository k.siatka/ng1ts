import { module } from 'angular';

import { TitleService } from './core/title.service';
import { AppComponent } from './app.component';

export const app = module('app', []);

app.service('TitleService', TitleService);
app.component('myApp', new AppComponent());
